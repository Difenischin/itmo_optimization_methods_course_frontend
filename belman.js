function getStartTable() {
    const table = [[35, 30, 40],
        [95, 110, 90],
        [270, 385, 395],
        [630, 470, 440],
        [700, 740, 620],
        [920, 800, 850],
        [980, 1170, 1000],
        [1040, 1150, 1080]
    ];
    return table;
}

function getBelmanResult(table, countInvest, company) {
    const firstIteration = getLastIterations(0);
    const result = [firstIteration];
    for (let currentCompany = 1; currentCompany < table[0].length; ++currentCompany) {
        let iteration = getLastIterations(currentCompany, result[currentCompany - 1]);
        result.push(iteration);
    }
    console.log(result);
    let maxDuration = getMaxDuration(result[result.length - 1]);
    console.log(maxDuration);
    return result[company][countInvest];

    function getLastIterations(currentCompany, lastIteration) {
        let resultIteration = [];
        for (let j = 0; j < table.length; ++j) {
            let iteration = getIteration(j, currentCompany, lastIteration);
            resultIteration.push(iteration);
        }
        addDuration(resultIteration);
        return resultIteration;

        function addDuration(iterations) {
            for (let j = 1; j < iterations.length; ++j) {
                let p = iterations[j].target - iterations[0].target;
                let z = j;
                iterations[j].duration = p/z;
            }
        }

    }

    function getIteration(countInvest, company, lastIteration) {
        const target = getTarget();
        const result = {
            target: target.target,
            allInvest: countInvest,
            investForCurrent: target.investForCurrent,
            current: company
        };
        return result;

        function getTarget() {
            const result = {};
            if (isEmptyOrUndefined(lastIteration)) {
                result.target = table[countInvest][0];
                result.investForCurrent = countInvest;
            } else {
                let sum = getSum(countInvest, lastIteration);
                result.target = Math.max.apply(null, sum);
                result.investForCurrent = sum.indexOf(result.target);
            }
            return result;

            function getSum(countInvest, lastIteration) {
                let sumArray = [];
                for (let i = 0; i <= countInvest; ++i) {
                    let sum = table[i][company] + lastIteration[countInvest - i].target;
                    sumArray.push(sum);
                }
                return sumArray;
            }
        }

        function isEmptyOrUndefined(value) {
            return value === undefined || value === '';
        }
    }
}

function getMaxDuration(iterations){
    let maxIndex = 0;
    let max = 0;
    for (let j = 1; j < iterations.length; ++j) {
        if(max < iterations[j].duration){
            max = iterations[j].duration;
            maxIndex = j;
        }
    }
    return {
        countInvest: maxIndex,
        maxDuration:max
    };
}

describe("Тестирование алгоритма схемы Belman", function () {
    const table = getStartTable();
    it("Тестирование нулевых инвестиций", function () {
        const trueResult = {
            target: 35,
            allInvest: 0,
            investForCurrent: 0,
            current: 0
        };
        expect(getBelmanResult(table, 0, 0)).toEqual(trueResult);
    });
    it("Тестирование для первого предприятия", function () {
        const trueResult = {
            target: 95,
            allInvest: 1,
            investForCurrent: 1,
            current: 0,
            duration: 60
        };
        expect(getBelmanResult(table, 1, 0)).toEqual(trueResult);
    });
    it("Тестирования для первого предприятия, при количесве инвестиций 5", function () {
        const trueResult = {
            target: 920,
            allInvest: 5,
            investForCurrent: 5,
            current: 0,
            duration: 177
        };
        expect(getBelmanResult(table, 5, 0)).toEqual(trueResult);
    });
    it("Тестирование для второго предприятия", function () {
        const trueResult = {
            target: 65,
            allInvest: 0,
            investForCurrent: 0,
            current: 1,
        };
        expect(getBelmanResult(table, 0, 1)).toEqual(trueResult);
    });
    it("Тестирование для второго предприятия при инвестицияха равных 2", function () {
        const trueResult = {
            target: 420,
            allInvest: 2,
            investForCurrent: 2,
            current: 1,
            duration: 177.5
        };
        expect(getBelmanResult(table, 2, 1)).toEqual(trueResult);
    });
    it("Тестирование для второго предприятия при инвестицияха равных 5", function () {
        const trueResult = {
            target: 1055,
            allInvest: 5,
            investForCurrent: 0,
            current: 2,
            duration: 190
        };
        expect(getBelmanResult(table, 5, 2)).toEqual(trueResult);
    });
    it("Тестирование ответа на задание по лабораторной работе", function () {
        const trueResult = {
            target: 1410,
            allInvest: 7,
            investForCurrent: 0,
            current: 2,
            duration: 186.42857142857142
        };
        const belman = getBelmanResult(table, 7, 2);
        expect(belman).toEqual(trueResult);
    });
});