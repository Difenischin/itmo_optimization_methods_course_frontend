function createGraphFunction(dataset, leftBorder, rightBorder) {
    const margin = {top: 50, right: 150, bottom: 50, left: 50}
        , width = window.innerWidth - margin.left - margin.right
        , height = window.innerHeight - margin.top - margin.bottom;

    const xScale = getXScale(leftBorder, rightBorder);
    const yScale = getYScale(dataset);

    const graph = getGraphInPage();

    createXAxis(graph, xScale);
    createYAxis(graph, yScale);
    createGrid();
    addLineFunction(graph, dataset);

    graph.addLines = function (iterations) {
        for (let i = 0; i < iterations.length; i++) {
            addLines(iterations[i].left, '#a5dab2');
            addLines(iterations[i].right, '#389e98');
        }

        function addLines(line, color) {
            graph.append("line")
                .style("stroke", color)
                .style("stroke-width", "2")
                .attr("x1", xScale(line.point))
                .attr("y1", yScale(line.resultPoint))
                .attr("x2", xScale(line.x_pointForLeft))
                .attr("y2", yScale(line.y_pointForLeft));
            graph.append("line")
                .style("stroke", color)
                .style("stroke-width", "2")
                .attr("x1", xScale(line.point))
                .attr("y1", yScale(line.resultPoint))
                .attr("x2", xScale(line.x_pointForRight))
                .attr("y2", yScale(line.y_pointForRight));
        }
    };
    return graph;


    function getGraphInPage() {
        return d3.select("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    }

    function createXAxis(graph, xScale) {
        graph.append("g")
            .attr("class", "x-axis")
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(xScale));
    }

    function createYAxis(graph, yScale) {
        graph.append("g")
            .attr("class", "y-axis")
            .call(d3.axisLeft(yScale));
    }

    function getXScale(leftBorder, rightBorder) {
        const xScale = d3.scaleLinear()
            .domain([leftBorder, rightBorder]) // input
            .range([leftBorder, width]); // output
        return xScale;
    }

    function getYScale(dataSet) {
        const max = getMaxResult(dataSet);
        const min = getMinResult(dataSet);

        const yScale = d3.scaleLinear()
            .domain([getLeftRange(min.resultFunction), getRightRange(max.resultFunction)]) // input
            .range([height, 0]); // output
        return yScale;

        function getLeftRange(min) {
            if (0 > min) {
                return min * 5;
            }
            return min;
        }

        function getRightRange(max) {
            if (0 < max) {
                return max * 5;
            }
            return max;
        }

        function getMaxResult(dataset) {
            return dataset.reduce(function (prev, cur) {
                    return cur.resultFunction > prev.resultFunction ? cur : prev;
                },
                {resultFunction: -Infinity});
        }

        function getMinResult(dataset) {
            return dataset.reduce(function (prev, cur) {
                    return cur.resultFunction < prev.resultFunction ? cur : prev;
                },
                {resultFunction: Infinity});
        }
    }

    function getLineGenerator() {
        const line = d3.line()
            .x(function (d, i) {
                return xScale(d.value);
            })
            .y(function (d) {
                return yScale(d.resultFunction);
            })
            .curve(d3.curveMonotoneX);
        return line;
    }

    function createGrid() {
        d3.selectAll("g.x-axis g.tick")
            .append("line")
            .style("stroke", '#000')
            .style("stroke-width", "0.2")
            .attr("x1", 0)
            .attr("y1", 0)
            .attr("x2", 0)
            .attr("y2", -width);


        d3.selectAll("g.y-axis g.tick")
            .append("line")
            .style("stroke", '#000')
            .style("stroke-width", "0.2")
            .attr("x1", 0)
            .attr("y1", 0)
            .attr("x2", xScale(rightBorder))
            .attr("y2", 0);
    }

    function addLineFunction(graph, dataset) {
        graph.append("path")
            .datum(dataset)
            .attr("class", "line")
            .attr("d", getLineGenerator());
    }
}