$(function () {
    katex.render("y = \\alpha * sin(\\beta * x) - \\gamma * x", $('#function')[0]);
    const vue = new Vue({
        el: '#value-container',
        data: {
            leftBorder: -5,
            rightBorder: 9.7,
            alpha: 2.67,
            beta: 3.044,
            gamma: 2.25,
            countIterations: 180,
            result: '',
            f: '',
            graph: '',
            genetikSettings: {
                epsilon: 0.00001,
                maxCountIteration: 1000,
                allCountPopulation: 100,
                mutationStep: 4,
                countProtectedFromMutation: 15
            },
            geneticResult: {}
        },
        created: function () {
            this.f = (x) => {
                return this.alpha * Math.sin(this.beta * x) - this.gamma * x;
            };
            const dataSetNew = getDataFunction(this.leftBorder, this.rightBorder, this.f, this.countIterations);
            this.graph = createGraphFunction(dataSetNew, this.leftBorder, this.rightBorder);
            this.updateResults();
            this.updateGeneticResults(this, this.genetikSettings);
        },
        methods: {
            updateCanvas: function () {
                $('#graph').empty();
                const dataSetNew = getDataFunction(this.leftBorder, this.rightBorder, this.f, this.countIterations);
                this.graph = createGraphFunction(dataSetNew, this.leftBorder, this.rightBorder);
                this.updateResults();
            },
            updateResults: function () {
                const start = performance.now();
                const result = piyavskyMethod(this.leftBorder, this.rightBorder, this.f, this.countIterations);
                const end = performance.now();
                console.log("piyavskyMethod took " + (end - start) + " milliseconds.");
                this.graph.addLines(result.iterations);
                this.result = result;
                this.result.timeFind = end - start;
            },
            updateGeneticResults: function (thiss, settings) {
                const start = performance.now();
                this.geneticResult = genetic(thiss.leftBorder, thiss.rightBorder, thiss.f, settings);
                const end = performance.now();
                this.geneticResult.timeFind = end - start;
                console.log("Call to GenetiсResult took " + this.timeFind + " milliseconds.");
            },
        },
        watch: {
            genetikSettings: {
                handler: function (after, before) {
                    this.updateGeneticResults(this, after);
                },
                deep: true
            },
        }
    });

    const vueAnt = new Vue({
        el: '#ant-colony',
        data: {
            algoritmSettings:{
                start: 0,
                end: 15,
                startPheromone: 1,
                pheromoneInsertConstant: 0.18,
                pheromoneEvaporationConstant: 0.08,
                iterationsCount: 100,
                greediness: 2,
                herdInstinct: 2,
                antCount: 100,
            },
            result: {}
        },
        methods: {
            run: function () {
                this.result = antColony({}, kotlin, this.algoritmSettings);
            },
        },
    });
});

describe("Юнит тесты", function () {
    it("Тестирование метода на простой функции", function () {
        const f = (x) => {
            return 3 * x * x * x * x / 4 + 12 * x * x * x + 66 * x * x + 144 * x + 3;
        };
        const accuracy = 5;
        expect(piyavskyMethod(-7, -1, f).answer).toBeCloseTo(-6.000000, accuracy);
        expect(piyavskyMethod(-10, 10, f).answer).toBeCloseTo(-1.999999, accuracy);
        expect(piyavskyMethod(0, 1, f).answer).toBeCloseTo(0, accuracy);
        expect(piyavskyMethod(0, 10, f).answer).toBeCloseTo(0, accuracy);
        expect(piyavskyMethod(-1, 8, f).answer).toBeCloseTo(-1, accuracy);
    });
    it("Тестирование вывода значений, по которым можно построить график функции", function () {
        const f = getDefaultFunction();
        const data = getDataFunction(-2, 8, f, 80);
        expect(data[0].value).toBe(-1.875);
    });
    it("Тестирование метода на синусоидальной функции", function () {
        const f = getDefaultFunction();
        const accuracy = 5;
        expect(piyavskyMethod(-2.1, 6.9, f).answer).toBeCloseTo(6.7337948116381, accuracy);
    });

    function getDefaultFunction() {
        const al = 6.67;
        const bta = 5.387;
        const gama = 5.25;
        const f = (x) => {
            return al * Math.sin(bta * x) - gama * x;
        };
        return f;
    }
});