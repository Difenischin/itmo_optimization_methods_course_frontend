function piyavskyMethod(leftBorder, rightBorder, f, iterationCount = 100, accuracyTo = 0.000001) {
    let result = {
        answer: '',
        resultAnswer: '',
        countIterationForAccuracy: '',
        iterations: []
    };
    let leftPoint = leftBorder;
    let rightPoint = rightBorder;
    const lcipschitzConst = getLipscHhitzConst(leftBorder, rightBorder);
    for (let i = 0;  i < iterationCount; i++) {
        let resultIteration = getResultIteration(leftPoint, rightPoint, lcipschitzConst);
        result.iterations.push(resultIteration);
        let PIx = getPointIntersection(leftPoint, rightPoint, lcipschitzConst);
        let leftPIx = getPointIntersection(leftPoint, PIx, lcipschitzConst);
        let rightPIx = getPointIntersection(PIx, rightPoint, lcipschitzConst);
        if (f(leftPIx) <= f(rightPIx)) {
            rightPoint = PIx;
        } else {
            leftPoint = PIx;
        }
        if (isAccuracyAchieved(leftPIx, rightPIx, accuracyTo)) {
            result.countIterationForAccuracy = i + 1;
            break;
        }
    }
    result.answer = getPointIntersection(leftPoint, rightPoint, lcipschitzConst);
    result.resultAnswer = f(result.answer);
    return result;

    function getLipscHhitzConst(leftBorder, rightBorder) {
        let lipschitzConst = 0;
        let step = Math.abs(rightBorder - leftBorder) / 10000;
        for (let cur = leftBorder + step; cur <= rightBorder; cur += step) {
            let difference = f(cur) - f(cur - step);
            let tmpL = Math.abs(difference) / step;
            if (tmpL > lipschitzConst) {
                lipschitzConst = tmpL;
            }
        }
        return lipschitzConst;
    }

    function getPointIntersection(leftPoint, rightPoint, lcipschitzConst) {
        const funcAvg = (f(leftPoint) - f(rightPoint)) / (2 * lcipschitzConst);
        const pointAvg = (leftPoint + rightPoint) / 2;
        return funcAvg + pointAvg;
    }

    function getResultIteration(leftBorder, rightBorder, lcipschitzConst) {
        const pointIntersection = getPointIntersection(leftBorder, rightBorder, lcipschitzConst);
        const resultIteration = {
            pointIntersection: getPointIntersection(leftBorder, rightBorder, lcipschitzConst),
            leftArea: getPointIntersection(leftBorder, pointIntersection, lcipschitzConst),
            rightArea: getPointIntersection(pointIntersection, rightBorder, lcipschitzConst),
            lcipschitzConst: lcipschitzConst,
            left: getMinorant(leftBorder, rightBorder, lcipschitzConst),
            right: getMajorant(leftBorder, rightBorder, lcipschitzConst)
        };
        return resultIteration;

        function getMinorant(leftBorder, rightBorder, lcipschitzConst) {
            const pointIntersection = getPointIntersection(leftBorder, rightBorder, lcipschitzConst);
            const leftDistanceToPointIntersection = (pointIntersection - leftBorder);
            const minorant = {
                point: leftBorder,
                resultPoint: f(leftBorder),
                distanceToPointIntersection: leftDistanceToPointIntersection,
                x_pointForLeft: leftBorder - leftDistanceToPointIntersection,
                y_pointForLeft: f(leftBorder) - leftDistanceToPointIntersection * lcipschitzConst,
                x_pointForRight: pointIntersection,
                y_pointForRight: f(leftBorder) - leftDistanceToPointIntersection * lcipschitzConst
            };
            return minorant;
        }

        function getMajorant(leftBorder, rightBorder, lcipschitzConst) {
            const pointIntersection = getPointIntersection(leftBorder, rightBorder, lcipschitzConst);
            const rightDistanceToPointIntersection = (rightBorder - pointIntersection);
            const majorant = {
                point: rightBorder,
                resultPoint: f(rightBorder),
                distanceToPointIntersection: rightDistanceToPointIntersection,
                x_pointForLeft: pointIntersection,
                y_pointForLeft: f(rightBorder) - rightDistanceToPointIntersection * lcipschitzConst,
                x_pointForRight: rightBorder + rightDistanceToPointIntersection,
                y_pointForRight: f(rightBorder) - rightDistanceToPointIntersection * lcipschitzConst
            };
            return majorant;
        }
    }

    function isAccuracyAchieved(leftPIx, rightPIx, accuracyTo) {
        return Math.abs(rightPIx - leftPIx) < accuracyTo;
    }
}

function getDataFunction(leftBorder, rightBorder, customFunction, iterationCount) {
    const step = Math.abs(rightBorder - leftBorder) / iterationCount;
    const result = [];
    for (let i = 0; i < iterationCount; ++i) {
        let x = leftBorder + step * (i + 1);
        let obj = {
            value: x,
            resultFunction: customFunction(x)
        };
        result.push(obj);
    }
    return result;
}