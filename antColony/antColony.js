if (typeof kotlin === 'undefined') {
    throw new Error("Error loading module 'antColony'. Its dependency 'kotlin' was not found. Please, check whether 'kotlin' is loaded prior to 'antColony'.");
}
let antColony = function (_, Kotlin, algoritmSettings) {
    'use strict';
    let println = Kotlin.kotlin.io.println_s8jyv4$;
    let min = Kotlin.kotlin.collections.min_l63kqw$;
    let toString = Kotlin.toString;
    let average = Kotlin.kotlin.collections.average_l63kqw$;
    let max = Kotlin.kotlin.collections.max_l63kqw$;
    let equals = Kotlin.equals;
    let average_0 = Kotlin.kotlin.collections.average_plj8ka$;
    let IntRange = Kotlin.kotlin.ranges.IntRange;
    let toHashSet = Kotlin.kotlin.collections.toHashSet_7wnvza$;
    let kotlin_js_internal_IntCompanionObject = Kotlin.kotlin.js.internal.IntCompanionObject;
    let first = Kotlin.kotlin.collections.first_7wnvza$;
    let Unit = Kotlin.kotlin.Unit;
    let Triple = Kotlin.kotlin.Triple;
    let mutableListOf = Kotlin.kotlin.collections.mutableListOf_i5x0yv$;
    let emptyList = Kotlin.kotlin.collections.emptyList_287e2$;
    let to = Kotlin.kotlin.to_ujzrz7$;
    let ensureNotNull = Kotlin.ensureNotNull;
    let windowed = Kotlin.kotlin.collections.windowed_au5p4$;
    let until = Kotlin.kotlin.ranges.until_dqglrj$;
    let CoroutineImpl = Kotlin.kotlin.coroutines.experimental.CoroutineImpl;
    let COROUTINE_SUSPENDED = Kotlin.kotlin.coroutines.experimental.intrinsics.COROUTINE_SUSPENDED;
    let buildSequence = Kotlin.kotlin.coroutines.experimental.buildSequence_of7nec$;
    let toList = Kotlin.kotlin.sequences.toList_veqyi0$;
    let numberToInt = Kotlin.numberToInt;
    let Kind_CLASS = Kotlin.Kind.CLASS;

    let start = algoritmSettings.start;
    let end = algoritmSettings.end;
    let startPosition = start;
    let endPosition = end;
    let startPheromone = algoritmSettings.startPheromone;
    let pheromoneInsertConstant = algoritmSettings.pheromoneInsertConstant;
    let pheromoneEvaporationConstant = algoritmSettings.pheromoneEvaporationConstant;
    let iterationsCount = algoritmSettings.iterationsCount;
    let greediness = algoritmSettings.greediness;
    let herdInstinct = algoritmSettings.herdInstinct;
    let antCount = algoritmSettings.antCount;

    let ArrayList_init = Kotlin.kotlin.collections.ArrayList_init_ww73n8$;
    let collectionSizeOrDefault = Kotlin.kotlin.collections.collectionSizeOrDefault_ba2ldo$;
    let Collection = Kotlin.kotlin.collections.Collection;

    function main(args) {
        let graph = initGraph();
        println(graph);
        let dijkstra = solveDijkstra(graph);
        println('Время алгоритма Дейскстры: ' + dijkstra.third);
        println('Кратчайший путь: ' + dijkstra.first);
        let tmp$;
        let sum = 0;
        tmp$ = dijkstra.second.iterator();
        while (tmp$.hasNext()) {
            let element = tmp$.next();
            sum = sum + element.weight | 0;
        }
        let optimalWeight = sum;
        println('Сумарный вес оптимального маршрута: ' + optimalWeight);
        let list = ArrayList_init(100);
        let tmp$_0;
        tmp$_0 = 100 - 1 | 0;
        for (let index = 0; index <= tmp$_0; index++) {
            list.add_11rb$(solveAnt(graph));
        }
        let antSolutions = list;
        let destination = ArrayList_init(collectionSizeOrDefault(antSolutions, 10));
        let tmp$_1;
        tmp$_1 = antSolutions.iterator();
        while (tmp$_1.hasNext()) {
            let item = tmp$_1.next();
            destination.add_11rb$(item.third);
        }
        println('Минимальное время нахождения маршрута муравьинной колонией: ' + toString(min(destination)));
        let destination_0 = ArrayList_init(collectionSizeOrDefault(antSolutions, 10));
        let tmp$_2;
        tmp$_2 = antSolutions.iterator();
        while (tmp$_2.hasNext()) {
            let item_0 = tmp$_2.next();
            destination_0.add_11rb$(item_0.third);
        }
        println('Среднее время нахождения маршрута муравьинной колонией: ' + average(destination_0));
        let destination_1 = ArrayList_init(collectionSizeOrDefault(antSolutions, 10));
        let tmp$_3;
        tmp$_3 = antSolutions.iterator();
        while (tmp$_3.hasNext()) {
            let item_1 = tmp$_3.next();
            destination_1.add_11rb$(item_1.third);
        }
        println('Максимальное время нахождения маршрута муравьинной колонией: ' + toString(max(destination_1)));
        let destination_2 = ArrayList_init(collectionSizeOrDefault(antSolutions, 10));
        let tmp$_4;
        tmp$_4 = antSolutions.iterator();
        while (tmp$_4.hasNext()) {
            let item_2 = tmp$_4.next();
            destination_2.add_11rb$(item_2.first);
        }
        let tmp$result;
        tmp$break: do {
            let tmp$_5;
            if (Kotlin.isType(destination_2, Collection) && destination_2.isEmpty()) {
                tmp$result = 0;
                break tmp$break;
            }
            let count = 0;
            tmp$_5 = destination_2.iterator();
            while (tmp$_5.hasNext()) {
                let element_0 = tmp$_5.next();
                if (equals(element_0, dijkstra.first)) {
                    count = count + 1 | 0;
                }
            }
            tmp$result = count;
        }
        while (false);
        println('Соотношение правильных решений: ' + tmp$result / antSolutions.size);
        let destination_3 = ArrayList_init(collectionSizeOrDefault(antSolutions, 10));
        let tmp$_6;
        tmp$_6 = antSolutions.iterator();
        while (tmp$_6.hasNext()) {
            let item_3 = tmp$_6.next();
            destination_3.add_11rb$(item_3.second);
        }
        let destination_4 = ArrayList_init(collectionSizeOrDefault(destination_3, 10));
        let tmp$_7;
        tmp$_7 = destination_3.iterator();
        while (tmp$_7.hasNext()) {
            let item_4 = tmp$_7.next();
            let tmp$_8 = destination_4.add_11rb$;
            let tmp$_9;
            let sum_0 = 0;
            tmp$_9 = item_4.iterator();
            while (tmp$_9.hasNext()) {
                let element_1 = tmp$_9.next();
                sum_0 = sum_0 + element_1.weight | 0;
            }
            tmp$_8.call(destination_4, sum_0);
        }
        let averageAntWeight = average_0(destination_4);
        println('Средняя абсолютная разница между оптимальным весом: ' + (averageAntWeight - optimalWeight));
        println('Средняя относительная разница между оптимальным весом: ' + (averageAntWeight / optimalWeight - 1));
    }

    function solveDijkstra(graph) {
        let before = (new Date()).getTime();
        let q = toHashSet(new IntRange(start, end));
        let previous = Kotlin.newArray(end + 1 | 0, null);
        let array = new Int32Array(end + 1 | 0);
        let tmp$;
        tmp$ = array.length - 1 | 0;
        for (let i = 0; i <= tmp$; i++) {
            array[i] = kotlin_js_internal_IntCompanionObject.MAX_VALUE;
        }
        let distances = array;
        distances[startPosition] = 0;
        while (!q.isEmpty()) {
            let currentVertex = {v: first(q)};
            let tmp$_0;
            tmp$_0 = q.iterator();
            while (tmp$_0.hasNext()) {
                let element = tmp$_0.next();
                if (distances[element] < distances[currentVertex.v]) {
                    currentVertex.v = element;
                }
            }
            q.remove_11rb$(currentVertex.v);
            let destination = ArrayList_init();
            let tmp$_1;
            tmp$_1 = graph.iterator();
            while (tmp$_1.hasNext()) {
                let element_0 = tmp$_1.next();
                if (element_0.isApplicable_za3lpa$(currentVertex.v)) {
                    destination.add_11rb$(element_0);
                }
            }
            let neighbors = destination;
            let tmp$_2;
            tmp$_2 = neighbors.iterator();
            while (tmp$_2.hasNext()) {
                let element_1 = tmp$_2.next();
                let altDistance = distances[currentVertex.v] + element_1.weight | 0;
                if (altDistance < distances[element_1.getOpposite_za3lpa$(currentVertex.v)]) {
                    distances[element_1.getOpposite_za3lpa$(currentVertex.v)] = altDistance;
                    previous[element_1.getOpposite_za3lpa$(currentVertex.v)] = currentVertex.v;
                }
            }
        }
        let after = (new Date()).getTime();
        let tmp$_3 = composeDijkstraSolution(previous, graph);
        let positions = tmp$_3.component1()
            , passed = tmp$_3.component2();
        return new Triple(positions, passed, after - before);
    }

    function composeDijkstraSolution$lambda(closure$graph) {
        return function (f) {
            let from = f.get_za3lpa$(0);
            let to = f.get_za3lpa$(1);
            let $receiver = closure$graph;
            let firstOrNull$result;
            firstOrNull$break: do {
                let tmp$;
                tmp$ = $receiver.iterator();
                while (tmp$.hasNext()) {
                    let element = tmp$.next();
                    if (element.matches_vux9f0$(from, to)) {
                        firstOrNull$result = element;
                        break firstOrNull$break;
                    }
                }
                firstOrNull$result = null;
            }
            while (false);
            return ensureNotNull(firstOrNull$result);
        };
    }

    function composeDijkstraSolution(references, graph) {
        let current = endPosition;
        let passedPositions = mutableListOf([current]);
        while (current !== startPosition) {
            let previous = references[current];
            if (previous != null) {
                passedPositions.add_wxm5ur$(0, previous);
                current = previous;
            }
            else {
                println('Something went wrong');
                return to(emptyList(), emptyList());
            }
        }
        let passed = windowed(passedPositions, 2, void 0, void 0, composeDijkstraSolution$lambda(graph));
        return to(passedPositions, passed);
    }

    function solveAnt(graph) {
        let tmp$;
        tmp$ = graph.iterator();
        while (tmp$.hasNext()) {
            let element = tmp$.next();
            element.pheromone = startPheromone;
        }
        let before = (new Date()).getTime();
        let $receiver = until(0, antCount);
        let destination = ArrayList_init(collectionSizeOrDefault($receiver, 10));
        let tmp$_0;
        tmp$_0 = $receiver.iterator();
        while (tmp$_0.hasNext()) {
            let item = tmp$_0.next();
            destination.add_11rb$(new Ant());
        }
        let ants = destination;
        let tmp$_1;
        tmp$_1 = iterationsCount - 1 | 0;
        for (let index = 0; index <= tmp$_1; index++) {
            let tmp$_2;
            tmp$_2 = ants.iterator();
            while (tmp$_2.hasNext()) {
                let element_0 = tmp$_2.next();
                element_0.move_4thu1e$(graph);
            }
            let tmp$_3;
            tmp$_3 = graph.iterator();
            while (tmp$_3.hasNext()) {
                let element_1 = tmp$_3.next();
                element_1.pheromone = element_1.pheromone * (1 - pheromoneEvaporationConstant);
            }
        }
        let after = (new Date()).getTime();
        let tmp$_4 = composeAntSolution(graph);
        let positions = tmp$_4.component1()
            , passed = tmp$_4.component2();
        return new Triple(positions, passed, after - before);
    }

    function composeAntSolution(graph) {
        let current = {v: startPosition};
        let passedPositions = mutableListOf([current.v]);
        let passed = ArrayList_init();
        while (current.v !== endPosition) {
            let destination = ArrayList_init();
            let tmp$;
            tmp$ = graph.iterator();
            while (tmp$.hasNext()) {
                let element = tmp$.next();
                if (!passed.contains_11rb$(element) && element.isApplicable_za3lpa$(current.v)) {
                    destination.add_11rb$(element);
                }
            }
            let tmp$result;
            tmp$break: do {
                let iterator = destination.iterator();
                if (!iterator.hasNext()) {
                    tmp$result = null;
                    break tmp$break;
                }
                let maxElem = iterator.next();
                let maxValue = maxElem.pheromone;
                while (iterator.hasNext()) {
                    let e = iterator.next();
                    let v = e.pheromone;
                    if (Kotlin.compareTo(maxValue, v) < 0) {
                        maxElem = e;
                        maxValue = v;
                    }
                }
                tmp$result = maxElem;
            }
            while (false);
            let edge = tmp$result;
            if (edge != null) {
                passed.add_11rb$(edge);
                current.v = edge.getOpposite_za3lpa$(current.v);
                let element_0 = current.v;
                passedPositions.add_11rb$(element_0);
            }
            else {
                println('Something went wrong');
                return to(emptyList(), emptyList());
            }
        }
        return to(passedPositions, passed);
    }

    function initGraph$lambda($receiver_0, continuation_0, suspended) {
        let instance = new Coroutine$initGraph$lambda($receiver_0, this, continuation_0);
        if (suspended) {
            return instance;
        }
        else {
            return instance.doResume(null);
        }
    }

    function Coroutine$initGraph$lambda($receiver_0, controller, continuation_0) {
        CoroutineImpl.call(this, continuation_0);
        this.$controller = controller;
        this.exceptionState_0 = 1;
        this.local$tmp$ = void 0;
        this.local$element = void 0;
        this.local$closure$continuation = void 0;
        this.local$tmp$_0 = void 0;
        this.local$$receiver = $receiver_0;
    }

    Coroutine$initGraph$lambda.$metadata$ = {
        kind: Kotlin.Kind.CLASS,
        simpleName: null,
        interfaces: [CoroutineImpl]
    };
    Coroutine$initGraph$lambda.prototype = Object.create(CoroutineImpl.prototype);
    Coroutine$initGraph$lambda.prototype.constructor = Coroutine$initGraph$lambda;
    Coroutine$initGraph$lambda.prototype.doResume = function () {
        do {
            try {
                switch (this.state_0) {
                    case 0:
                        this.local$tmp$ = (new IntRange(start, end)).iterator();
                        this.state_0 = 2;
                        continue;
                    case 1:
                        throw this.exception_0;
                    case 2:
                        if (!this.local$tmp$.hasNext()) {
                            this.state_0 = 6;
                            continue;
                        }

                        this.local$element = this.local$tmp$.next();
                        this.local$closure$continuation = this;
                        let $receiver = new IntRange(this.local$element, end);
                        let destination = ArrayList_init();
                        let tmp$;
                        tmp$ = $receiver.iterator();
                        while (tmp$.hasNext()) {
                            let element = tmp$.next();
                            if (this.local$element !== element) {
                                destination.add_11rb$(element);
                            }
                        }

                        this.local$tmp$_0 = destination.iterator();
                        this.state_0 = 3;
                        continue;
                    case 3:
                        if (!this.local$tmp$_0.hasNext()) {
                            this.state_0 = 5;
                            continue;
                        }

                        let element_0 = this.local$tmp$_0.next();
                        this.state_0 = 4;
                        this.result_0 = this.local$$receiver.yield_11rb$(new Edge(this.local$element, element_0, getRandomInt(0, 200) + 21 | 0), this.local$closure$continuation);
                        if (this.result_0 === COROUTINE_SUSPENDED) {
                            return COROUTINE_SUSPENDED;
                        }
                        break;
                    case 4:
                        this.state_0 = 3;
                        continue;
                    case 5:
                        this.state_0 = 2;
                        continue;
                    case 6:
                        return Unit;
                }
            }
            catch (e) {
                if (this.state_0 === 1) {
                    throw e;
                }
                else {
                    this.state_0 = this.exceptionState_0;
                    this.exception_0 = e;
                }
            }
        }
        while (true);
    };

    function initGraph() {
        return toList(buildSequence(initGraph$lambda));
    }

    let Math_0 = Math;

    function getRandomInt(min, max) {
        let x = Math.random() * (max - min + 1 | 0);
        let a = Math_0.floor(x);
        let b = numberToInt(a);
        let c = b + min | 0;
        return c;
    }

    function Edge(from, to, weight, pheromone) {
        if (pheromone === void 0) {
            pheromone = startPheromone;
        }
        this.from_0 = from;
        this.to_0 = to;
        this.weight = weight;
        this.pheromone = pheromone;
    }

    Edge.prototype.matches_vux9f0$ = function (from, to) {
        return from === this.from_0 && to === this.to_0 || (from === this.to_0 && to === this.from_0);
    };
    Edge.prototype.isApplicable_za3lpa$ = function (point) {
        return point === this.from_0 || point === this.to_0;
    };
    Edge.prototype.getOpposite_za3lpa$ = function (point) {
        return point === this.from_0 ? this.to_0 : point === this.to_0 ? this.from_0 : -1;
    };
    Edge.prototype.getProbability_4thu1e$ = function (edges) {
        let tmp$ = this.getProbabilityConstant_0();
        let tmp$_0;
        let sum = 0.0;
        tmp$_0 = edges.iterator();
        while (tmp$_0.hasNext()) {
            let element = tmp$_0.next();
            sum += element.getProbabilityConstant_0();
        }
        return tmp$ / sum;
    };
    Edge.prototype.toString = function () {
        return '(from: ' + this.from_0 + ', to: ' + this.to_0 + ', w: ' + this.weight + ')';
    };
    Edge.prototype.getProbabilityConstant_0 = function () {
        let $receiver = this.pheromone;
        let n = herdInstinct;
        let tmp$ = Math_0.pow($receiver, n);
        let $receiver_0 = 1.0 / this.weight;
        let n_0 = greediness;
        return tmp$ * Math_0.pow($receiver_0, n_0);
    };
    Edge.$metadata$ = {
        kind: Kind_CLASS,
        simpleName: 'Edge',
        interfaces: []
    };

    function Ant(currentPosition, passedEdges) {
        if (currentPosition === void 0) {
            currentPosition = startPosition;
        }
        if (passedEdges === void 0) {
            passedEdges = ArrayList_init();
        }
        this.currentPosition_0 = currentPosition;
        this.passedEdges_0 = passedEdges;
    }

    Ant.prototype.move_4thu1e$ = function (graph) {
        let destination = ArrayList_init();
        let tmp$;
        tmp$ = graph.iterator();
        while (tmp$.hasNext()) {
            let element = tmp$.next();
            if (element.isApplicable_za3lpa$(this.currentPosition_0)) {
                destination.add_11rb$(element);
            }
        }
        let destination_0 = ArrayList_init();
        let tmp$_0;
        tmp$_0 = destination.iterator();
        while (tmp$_0.hasNext()) {
            let element_0 = tmp$_0.next();
            if (!this.passedEdges_0.contains_11rb$(element_0)) {
                destination_0.add_11rb$(element_0);
            }
        }
        let edges = destination_0;
        let maxBy$result;
        maxBy$break: do {
            let iterator = edges.iterator();
            if (!iterator.hasNext()) {
                maxBy$result = null;
                break maxBy$break;
            }
            let maxElem = iterator.next();
            let maxValue = maxElem.getProbability_4thu1e$(edges) * Math.random();
            while (iterator.hasNext()) {
                let e = iterator.next();
                let v = e.getProbability_4thu1e$(edges) * Math.random();
                if (Kotlin.compareTo(maxValue, v) < 0) {
                    maxElem = e;
                    maxValue = v;
                }
            }
            maxBy$result = maxElem;
        }
        while (false);
        let nextEdge = maxBy$result;
        if (nextEdge == null) {
            this.restart_0();
            return;
        }
        this.currentPosition_0 = nextEdge.getOpposite_za3lpa$(this.currentPosition_0);
        this.passedEdges_0.add_11rb$(nextEdge);
        if (this.currentPosition_0 === endPosition) {
            this.putPheromone_0();
            this.restart_0();
        }
    };
    Ant.prototype.putPheromone_0 = function () {
        let tmp$;
        tmp$ = this.passedEdges_0.iterator();
        while (tmp$.hasNext()) {
            let element = tmp$.next();
            let tmp$_0 = element.pheromone;
            let tmp$_1 = pheromoneInsertConstant;
            let tmp$_2;
            let sum = 0;
            tmp$_2 = this.passedEdges_0.iterator();
            while (tmp$_2.hasNext()) {
                let element_0 = tmp$_2.next();
                sum = sum + element_0.weight | 0;
            }
            element.pheromone = tmp$_0 + tmp$_1 / sum;
        }
    };
    Ant.prototype.restart_0 = function () {
        this.currentPosition_0 = startPosition;
        this.passedEdges_0.clear();
    };
    Ant.$metadata$ = {
        kind: Kind_CLASS,
        simpleName: 'Ant',
        interfaces: []
    };
    Object.defineProperty(_, 'start', {
        get: function () {
            return start;
        }
    });
    Object.defineProperty(_, 'end', {
        get: function () {
            return end;
        }
    });
    Object.defineProperty(_, 'startPosition', {
        get: function () {
            return startPosition;
        }
    });
    Object.defineProperty(_, 'endPosition', {
        get: function () {
            return endPosition;
        }
    });
    Object.defineProperty(_, 'random', {
        get: function () {
            return random;
        }
    });
    Object.defineProperty(_, 'startPheromone', {
        get: function () {
            return startPheromone;
        }
    });
    Object.defineProperty(_, 'pheromoneInsertConstant', {
        get: function () {
            return pheromoneInsertConstant;
        }
    });
    Object.defineProperty(_, 'pheromoneEvaporationConstant', {
        get: function () {
            return pheromoneEvaporationConstant;
        }
    });
    Object.defineProperty(_, 'iterationsCount', {
        get: function () {
            return iterationsCount;
        }
    });
    Object.defineProperty(_, 'greediness', {
        get: function () {
            return greediness;
        }
    });
    Object.defineProperty(_, 'herdInstinct', {
        get: function () {
            return herdInstinct;
        }
    });
    Object.defineProperty(_, 'antCount', {
        get: function () {
            return antCount;
        }
    });
    _.main_kand9s$ = main;
    _.solveDijkstra_4thu1e$ = solveDijkstra;
    _.composeDijkstraSolution_8shzg7$ = composeDijkstraSolution;
    _.solveAnt_4thu1e$ = solveAnt;
    _.composeAntSolution_4thu1e$ = composeAntSolution;
    _.initGraph = initGraph;
    _.getRandomInt_vux9f0$ = getRandomInt;
    _.Edge = Edge;
    _.Ant = Ant;

    main([]);
    Kotlin.defineModule('antColony', _);
    return _;
};
