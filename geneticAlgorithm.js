function genetic(leftBorder, rightBorder, func, settings) {
    /*const settings = {
        epsilon: 0.00001,
        maxCountIteration: 1000,
        allCountPopulation: 100,
        mutationStep: 4,
        countProtectedFromMutation: 15
    };*/
    let resultIterations = [];
    let populations = getStartPopulations(leftBorder, rightBorder);
    sort(populations);
    resultIterations.push(getClone(populations));
    let iteration = 0;
    for (; iteration < settings.maxCountIteration; iteration++) {
        crossover(populations, settings, leftBorder, rightBorder, iteration);
        sort(populations);
        resultIterations.push(getClone(populations));
        if (isStoppingCriterion(iteration, resultIterations)) {
            break;
        }
    }
    console.log(resultIterations);
    console.log(iteration);
    const result = populations[0];
    result.resultIterations = resultIterations;
    return result;


    function getStartPopulations(leftBorder, rightBorder) {
        let populations = [];
        for (let i = 0; i < settings.allCountPopulation; i++) {
            populations.push(getPopulation(leftBorder, rightBorder));
        }
        return populations;

        function getPopulation(leftBorder, rightBorder) {
            let value = mutation(leftBorder, rightBorder);
            let population = {
                value: value,
                resultFunction: func(value)
            };
            return population;
        }
    }

    function sort(populations) {
        for (let i = 0; i < populations.length; i++) {
            for (let j = i + 1; j < populations.length; j++) {
                if (isCanSort(populations[j].resultFunction, populations[i].resultFunction)) {
                    let buffer = getClone(populations[i]);
                    populations[i] = getClone(populations[j]);
                    populations[j] = getClone(buffer);
                }
            }
        }

        function isCanSort(min, max) {
            return Math.abs(min) > Math.abs(max);
        }
    }

    function getClone(obj) {
        return JSON.parse(JSON.stringify(obj));
    }

    function isStoppingCriterion(iteration, resultIterations) {
        if (iteration % (settings.mutationStep - 1) === 0) {
            let a = resultIterations[iteration][0].resultFunction;
            let b = resultIterations[iteration + 1][0].resultFunction;
            let difference = a - b;
            if (Math.abs(difference) < settings.epsilon) {
                return true;
            }
        }
        return false;
    }

    function crossover(population, settings, leftBorder, rightBorder, iter)  // кроссовер: среднее арифметическое
    {
        if (iter % settings.mutationStep === 0) {
            crossoverMutationOff();
        } else {
            crossoverMutationOff();
            crossoverMutationOn();
        }

        function crossoverMutationOff() {
            let k = settings.allCountPopulation - 1;
            for (let i = 0; i < settings.countProtectedFromMutation; i++) {
                population[k].value = (population[i].value + population[i + 1].value) / 2;
                population[k].resultFunction = func(population[k].value);
                k--;
            }
            for (let i = 0; i < population.length; i++) {
                population[i].value = inversion(population[i].value, settings.epsilon, leftBorder, rightBorder);
                population[i].resultFunction = func(population[k].value);
            }

            function inversion(x, eps, leftBorder, rightBorder)  // инверсия: поиск в окрестностях точки
            {
                getSignalForInversion().setNext();
                let result = x + getLocalInversion(eps);
                if (getSignalForInversion().getValue() % 2 === 0) {
                    result = x - getLocalInversion(eps);
                }
                if (result < leftBorder) {
                    result = leftBorder;
                }
                if (result > rightBorder) {
                    result = rightBorder;
                }
                return result;

                function getLocalInversion(eps) {
                    return eps * Math.random() * 10;
                }
            }
        }

        function crossoverMutationOn() {
            for (let i = 0; i < getIndexEndMicroMutation(); i++) {
                population[i].value = getMicroMutation(population[i].value, leftBorder, rightBorder);
                population[i].resultFunction = func(population[i].value);
            }
            for (let i = getIndexEndMicroMutation() - 1; i < getIndexEndMutation(); i++) {
                population[i].value = mutation(leftBorder, rightBorder);
                population[i].resultFunction = func(population[i].value);
            }

            function getIndexEndMicroMutation() {
                const difference = getIndexEndMutation();
                if (difference % 2 === 0) {
                    return difference / 2;
                } else {
                    return (difference + 1) / 2;
                }
            }

            function getIndexEndMutation() {
                const difference = settings.allCountPopulation - settings.countProtectedFromMutation;
                return difference;
            }
        }
    }

    function mutation(leftBorder, rightBorder) {
        return getRandomArbitrary(leftBorder, rightBorder);

        function getRandomArbitrary(min, max) {
            return Math.random() * (max - min) + min;
        }
    }

    function getMicroMutation(point, leftBorder, rightBorder) {
        let result = point + Math.random();
        if (getSignalForInversion().getValue() % 2 !== 0) {
            result = point - Math.random();
        }
        if (result < leftBorder) {
            result = leftBorder;
        }
        if (result > rightBorder) {
            result = rightBorder;
        }
        return result;
    }
}

function getSignalForInversion() {
    if (getSignalForInversion.instance) {
        return getSignalForInversion.instance;
    }
    let sign = 0;
    getSignalForInversion.instance = {
        setNext: () => {
            sign++;
        },
        getValue: () => {
            return sign;
        }
    };
    return getSignalForInversion.instance;
}

describe("Тестирование генетического алгоритма", function () {
    it("Общее тестировнаие", function () {
        const result = genetic(-5, 9.7, getDefaultFunction(), getDefaultSettings());
        expect(result.value).toEqual(9.7);
        expect(result.resultFunction).toEqual(-24.36085747755147);
    });
    it("Посик значения в узкой области", function () {
        const result = genetic(9.5, 9.7, getDefaultFunction(), getDefaultSettings());
        expect(result.value).toEqual(9.7);
        expect(result.resultFunction).toEqual(-24.36085747755147);
    });

    function getDefaultSettings() {
        const settings = {
            epsilon: 0.00001,
            maxCountIteration: 1000,
            allCountPopulation: 100,
            mutationStep: 4,
            countProtectedFromMutation: 15
        };
        return settings;
    }

    function getDefaultFunction() {
        const al = 2.67;
        const bta = 3.044;
        const gama = 2.25;
        const f = (x) => {
            return al * Math.sin(bta * x) - gama * x;
        };
        return f;
    }
});